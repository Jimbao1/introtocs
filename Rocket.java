//Purpose : Rocket
//Programmer : Jhann
//Date : August 28 2019

public class Rocket
{
		public static void main (String[] args )
		{
		System.out.println("               /\\");
		System.out.println("              //\\\\");
		System.out.println("             ///\\\\\\");
		System.out.println("            ////\\\\\\\\");
		System.out.println("           ////  \\\\\\\\");
		System.out.println("          ////    \\\\\\\\");
		System.out.println("         ////      \\\\\\\\");
		System.out.println("        ////        \\\\\\\\");
		System.out.println("       ////          \\\\\\\\");
		System.out.println("      ////============\\\\\\\\");
		System.out.println("     /|||==============|||\\");
		System.out.println("      |||              |||");
		System.out.println("      |||  HH      HH  |||");
		System.out.println("      |||  HH      HH  |||");
	    System.out.println("      |||              |||");
	    System.out.println("      |||     \"7\"      |||");
	    System.out.println("      |||   =======    |||");
		System.out.println("      |||   |  |  |    |||");
		System.out.println("      |||   | O|O |    |||");
		System.out.println("      |||   |__|__|    |||");
		System.out.println("   ==========================");
		System.out.println("   //////////////////////////");
		System.out.println("  ||||||||||||||||||||||||||");
		System.out.println(" //////////////////////////");
		System.out.println("|||||||||||||||||||||||||");
	}

}
