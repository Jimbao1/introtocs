//Purpose : StringLiteral
//Programmer : Jhann
//Date : August 31 2019

public class StringLiteral
{
	public static void main(String[] args)
	{

	// 4 System.out.println statements
	System.out.println("In Java, a String literal is a sequence of character inside");
	System.out.println("a pair of double quotation marks, such as \"Java is fun\".");
	System.out.println("Java programmers should understand the difference between \"");
	System.out.println("and \\\" as well as the difference between \' and \".");

	// Space between paragraphs
	System.out.println("");

	// 1 System.out.println statement
	System.out.println("In Java, a String literal is a sequence of characters inside\n"
	+ "a pair of double quotation marks, such as \"Java is fun\".\n"
	+ "Java programmers should understand the difference between \"\n"
	+ "and \\\" as well as the difference between \' and \".");

	}
}