//Purpose : Coins
//Programmer : Jhann
//Date : August 31 2019

import java.util.Scanner;

public class Coins
{
	public static void main (String [] args)
	{
		Scanner keyboard = new Scanner (System.in);

		// setting the divisors as a fixed number
		final int TooniesDivisor = 200;
		final int LooniesDivisor = 100;
		final int QuartersDivisor = 25;
		final int DimesDivisor = 10;
		final int NickelsDivisor = 5;
		final int CentsDivisor = 1;

		// enter the amount of coins
		System.out.print("Enter change in cents : ");
		int coins = keyboard.nextInt();

		// toonies and remaining toonies
		int toonies = coins / TooniesDivisor;
		int rToonies = coins % TooniesDivisor;

		// loonies and remaining loonies
		int loonies = rToonies / LooniesDivisor;
		int rLoonies = rToonies % LooniesDivisor;

		// quarters and remaining quarters
		int quarters = rLoonies / QuartersDivisor;
		int rQuarters = rLoonies % QuartersDivisor;

		// dimes and dimes remaining
		int dimes = rQuarters / DimesDivisor;
		int rDimes = rQuarters % DimesDivisor;

		// nickels and nickels remaining
		int nickels = rDimes / NickelsDivisor;
		int rNickels = rDimes % NickelsDivisor;

		// pennies
		int pennies = rNickels / CentsDivisor;

		// minimum amount of coins needed
		System.out.println("A minimum of "
		+ (toonies+loonies+quarters+dimes+nickels+pennies)
		+ " coins to make a change for " + coins + " cents: ");

		// displays the amount of coins required
		System.out.println("Toonies : " + toonies);
		System.out.println("Loonies : " + loonies);
		System.out.println("Quarters : " + quarters);
		System.out.println("Dimes : " + dimes);
		System.out.println("Nickels : " + nickels);
		System.out.println("Cents : " + pennies);
	}
}
