//Purpose : Future Financial Return Report
//Programmer : Jhann
//Date : September 4 2019

import java.util.Scanner;
import java.text.NumberFormat;

public class FutureFinancialReport
{
	public static void main (String[] args)
	{
		Scanner keyboard = new Scanner (System.in);

		//Prompt the user to enter an investment amount
		System.out.print("Enter investment amount: ");
		double investment = keyboard.nextDouble();

		//Prompt the user to enter an annual interest rate
		System.out.print("Enter annual interest rate : ");
		double interest = keyboard.nextDouble();

		//Output header
		System.out.println("Report on investment in 5, 10 , and 20 years ");
		System.out.println("-------------------------------------------- ");

		//Format the currency and percent
		NumberFormat cf = NumberFormat.getCurrencyInstance();
		System.out.println("Investment : " + cf.format(investment));
		NumberFormat pf = NumberFormat.getPercentInstance();
		pf.setMinimumFractionDigits(1);
		System.out.println("Annual Interest rate : " + pf.format(interest));

		//Compute future value in 5 years
		double InterestRate5 = Math.pow(interest + 1.0,  5.0 );
		double ValueIn5Years = investment * InterestRate5;

		//Computer future value in 10 years
		double InterestRate10 = Math.pow(interest + 1.0, 10.0);
		double ValueIn10Years = investment * InterestRate10;

		//Computer future value in 20 years
		double InterestRate20 = Math.pow(interest + 1.0, 20.0);
		double ValueIn20Years = investment * InterestRate20;

		//Print the value in 5, 10 and 20 years
		System.out.println("Future value in 5 years : " + ValueIn5Years);
		System.out.println("Future value in 10 years : " + ValueIn10Years);
		System.out.println("Future value in 20 years : " + ValueIn20Years);
	}
}