import java.util.Scanner;

public class CelsiusToFahrenheit
{
	public static void main (String[] args)
	{
		Scanner analyze = new Scanner (System.in);

		// Formula for C to F
		// (C * 9/5) + 32 = F

		System.out.print("Enter a value in degrees Celsius : ");
		double celsius = analyze.nextDouble();
		double fahrenheit = celsius * 9/5 +32;
		System.out.println(celsius + " degrees Celsius is equivalent to " + fahrenheit + " degrees Farenheit.");
	}
}